from django.urls import path
from . import views

app_name = 'heroes'
urlpatterns = [
  path('', views.HeroList.as_view(), name='hero-list'),
  path('<int:pk>/', views.HeroDetail.as_view(), name='hero-detail'),
  path('create/', views.HeroCreate.as_view(), name='hero-create'),
  path('update/<int:pk>/', views.HeroUpdate.as_view(), name='hero-update'),
  path('delete/<int:pk>/', views.HeroDelete.as_view(), name='hero-delete'),
]
