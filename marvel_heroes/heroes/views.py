from django.shortcuts import render
from django.urls import reverse_lazy
from django.views import generic
from .models import Hero
from .forms import HeroForm

class HeroList(generic.ListView):
  model = Hero

class HeroDetail(generic.DetailView):
  model = Hero

class HeroCreate(generic.CreateView):
  model = Hero
  form_class = HeroForm
  success_url = reverse_lazy('heroes:hero-list')

class HeroUpdate(generic.UpdateView):
  model = Hero
  form_class = HeroForm
  success_url = reverse_lazy('heroes:hero-list')

class HeroDelete(generic.DeleteView):
  model = Hero
  form_class = HeroForm
  success_url = reverse_lazy('heroes:hero-list')
