from django.shortcuts import render
from django.urls import reverse_lazy
from django.views import generic
from .models import Villain
from .forms import VillainForm

class VillainList(generic.ListView):
  model = Villain

class VillainDetail(generic.DetailView):
  model = Villain

class VillainCreate(generic.CreateView):
  model = Villain
  form_class = VillainForm
  success_url = reverse_lazy('villains:villain-list')

class VillainUpdate(generic.UpdateView):
  model = Villain
  form_class = VillainForm
  success_url = reverse_lazy('villains:villain-list')

class VillainDelete(generic.DeleteView):
  model = Villain
  form_class = VillainForm
  success_url = reverse_lazy('villains:villain-list')
