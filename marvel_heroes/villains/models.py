from django.db import models

class Villain(models.Model):
  name = models.CharField(max_length=250)
  superpower = models.CharField(max_length=150)
  secret_identity = models.CharField(max_length=250)
  craeted_at = models.DateTimeField(auto_now_add=True, blank=True, null=True)
  updated_at = models.DateTimeField(auto_now=True)
  