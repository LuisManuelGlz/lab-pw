from django import forms
from .models import Villain

class VillainForm(forms.ModelForm):
  class Meta:
    model = Villain
    fields = ['name', 'superpower', 'secret_identity',]