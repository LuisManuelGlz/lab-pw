from django.urls import path
from . import views

app_name = 'villains'
urlpatterns = [
  path('', views.VillainList.as_view(), name='villain-list'),
  path('<int:pk>/', views.VillainDetail.as_view(), name='villain-detail'),
  path('create/', views.VillainCreate.as_view(), name='villain-create'),
  path('update/<int:pk>/', views.VillainUpdate.as_view(), name='villain-update'),
  path('delete/<int:pk>/', views.VillainDelete.as_view(), name='villain-delete'),
]
