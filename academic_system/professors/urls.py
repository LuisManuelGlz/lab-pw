from django.urls import path
from . import views

app_name = 'professors'
urlpatterns = [
  path('', views.ProfessorList.as_view(), name='professor-list'),
  path('<int:pk>/', views.ProfessorDetail.as_view(), name='professor-detail'),
  path('create/', views.ProfessorCreate.as_view(), name='professor-create'),
  path('update/<int:pk>/', views.ProfessorUpdate.as_view(), name='professor-update'),
  path('delete/<int:pk>/', views.ProfessorDelete.as_view(), name='professor-delete'),
]
