from django.shortcuts import render
from django.urls import reverse_lazy
from django.views import generic
from .models import Professor
from .forms import ProfessorForm

class ProfessorList(generic.ListView):
  model = Professor

class ProfessorDetail(generic.DetailView):
  model = Professor

class ProfessorCreate(generic.CreateView):
  model = Professor
  form_class = ProfessorForm
  success_url = reverse_lazy('professors:professor-list')

class ProfessorUpdate(generic.UpdateView):
  model = Professor
  form_class = ProfessorForm
  success_url = reverse_lazy('professors:professor-list')

class ProfessorDelete(generic.DeleteView):
  model = Professor
  form_class = ProfessorForm
  success_url = reverse_lazy('professors:professor-list')
