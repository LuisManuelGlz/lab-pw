from django.shortcuts import render
from django.urls import reverse_lazy
from django.views import generic
from .models import Student
from .forms import StudentForm

class StudentList(generic.ListView):
  model = Student

class StudentDetail(generic.DetailView):
  model = Student

class StudentCreate(generic.CreateView):
  model = Student
  form_class = StudentForm
  success_url = reverse_lazy('students:student-list')

class StudentUpdate(generic.UpdateView):
  model = Student
  form_class = StudentForm
  success_url = reverse_lazy('students:student-list')

class StudentDelete(generic.DeleteView):
  model = Student
  form_class = StudentForm
  success_url = reverse_lazy('students:student-list')
