from django.urls import path
from . import views

app_name = 'students'
urlpatterns = [
  path('', views.StudentList.as_view(), name='student-list'),
  path('<int:pk>/', views.StudentDetail.as_view(), name='student-detail'),
  path('create/', views.StudentCreate.as_view(), name='student-create'),
  path('update/<int:pk>/', views.StudentUpdate.as_view(), name='student-update'),
  path('delete/<int:pk>/', views.StudentDelete.as_view(), name='student-delete'),
]