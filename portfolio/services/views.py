from django.shortcuts import render
from django.urls import reverse_lazy
from django.views import generic
from .models import Service
from .forms import ServiceForm

class ServiceList(generic.ListView):
  model = Service

class ServiceDetail(generic.DetailView):
  model = Service

class ServiceCreate(generic.CreateView):
  model = Service
  form_class = ServiceForm
  success_url = reverse_lazy('services:service-list')

class ServiceUpdate(generic.UpdateView):
  model = Service
  form_class = ServiceForm
  success_url = reverse_lazy('services:service-list')

class ServiceDelete(generic.DeleteView):
  model = Service
  form_class = ServiceForm
  success_url = reverse_lazy('services:service-list')
