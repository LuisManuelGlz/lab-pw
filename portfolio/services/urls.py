from django.urls import path
from . import views

app_name = 'services'
urlpatterns = [
  path('', views.ServiceList.as_view(), name='service-list'),
  path('<int:pk>/', views.ServiceDetail.as_view(), name='service-detail'),
  path('create/', views.ServiceCreate.as_view(), name='service-create'),
  path('update/<int:pk>/', views.ServiceUpdate.as_view(), name='service-update'),
  path('delete/<int:pk>/', views.ServiceDelete.as_view(), name='service-delete'),
]