from django.db import models
from django.utils import timezone

class Service(models.Model):
  name = models.CharField(max_length=250)
  description = models.CharField(max_length=10, blank=True, null=True)
  price = models.DecimalField(max_digits=8, decimal_places=2)
  created_at = models.DateTimeField(auto_now_add=True)
  updated_at = models.DateTimeField(auto_now=True)
