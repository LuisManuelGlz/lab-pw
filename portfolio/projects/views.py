from django.shortcuts import render
from django.urls import reverse_lazy
from django.views import generic
from .models import Project
from .forms import ProjectForm

class ProjectList(generic.ListView):
  model = Project

class ProjectDetail(generic.DetailView):
  model = Project

class ProjectCreate(generic.CreateView):
  model = Project
  form_class = ProjectForm
  success_url = reverse_lazy('projects:project-list')

class ProjectUpdate(generic.UpdateView):
  model = Project
  form_class = ProjectForm
  success_url = reverse_lazy('projects:project-list')

class ProjectDelete(generic.DeleteView):
  model = Project
  form_class = ProjectForm
  success_url = reverse_lazy('projects:project-list')
