from django.urls import path
from . import views

app_name = 'projects'
urlpatterns = [
  path('', views.ProjectList.as_view(), name='project-list'),
  path('<int:pk>/', views.ProjectDetail.as_view(), name='project-detail'),
  path('create/', views.ProjectCreate.as_view(), name='project-create'),
  path('update/<int:pk>/', views.ProjectUpdate.as_view(), name='project-update'),
  path('delete/<int:pk>/', views.ProjectDelete.as_view(), name='project-delete'),
]