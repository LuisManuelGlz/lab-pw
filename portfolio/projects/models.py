from django.db import models
from django.utils import timezone

class Project(models.Model):
  name = models.CharField(max_length=250)
  description = models.TextField(default='')
  created_at = models.DateTimeField(auto_now_add=True)
  updated_at = models.DateTimeField(auto_now=True)
