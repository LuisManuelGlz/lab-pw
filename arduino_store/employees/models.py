from django.db import models

class Employee(models.Model):
  name = models.CharField(max_length=250)
  position = models.CharField(max_length=100)
  salary = models.DecimalField(max_digits=8, decimal_places=2)
