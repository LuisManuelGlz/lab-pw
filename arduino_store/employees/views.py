from django.shortcuts import render
from django.urls import reverse_lazy
from django.views import generic
from .models import Employee
from .forms import EmployeeForm

class EmployeeList(generic.ListView):
  model = Employee

class EmployeeDetail(generic.DetailView):
  model = Employee

class EmployeeCreate(generic.CreateView):
  model = Employee
  form_class = EmployeeForm
  success_url = reverse_lazy('employees:employee-list')

class EmployeeUpdate(generic.UpdateView):
  model = Employee
  form_class = EmployeeForm
  success_url = reverse_lazy('employees:employee-list')

class EmployeeDelete(generic.DeleteView):
  model = Employee
  form_class = EmployeeForm
  success_url = reverse_lazy('employees:employee-list')
