from django.urls import path
from . import views

app_name = 'employees'
urlpatterns = [
  path('', views.EmployeeList.as_view(), name='employee-list'),
  path('<int:pk>/', views.EmployeeDetail.as_view(), name='employee-detail'),
  path('create/', views.EmployeeCreate.as_view(), name='employee-create'),
  path('update/<int:pk>/', views.EmployeeUpdate.as_view(), name='employee-update'),
  path('delete/<int:pk>/', views.EmployeeDelete.as_view(), name='employee-delete'),
]