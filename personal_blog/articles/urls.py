from django.urls import path
from . import views

app_name = 'articles'
urlpatterns = [
  path('', views.ArticleList.as_view(), name='article-list'),
  path('<int:pk>/', views.ArticleDetail.as_view(), name='article-detail'),
  path('create/', views.ArticleCreate.as_view(), name='article-create'),
  path('update/<int:pk>/', views.ArticleUpdate.as_view(), name='article-update'),
  path('delete/<int:pk>/', views.ArticleDelete.as_view(), name='article-delete'),
]
