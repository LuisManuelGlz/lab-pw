from django.shortcuts import render
from django.contrib.auth.models import User
from django.urls import reverse_lazy
from django.views import generic
from .models import Article
from .forms import ArticleForm

class ArticleList(generic.ListView):
  model = Article

  def get_queryset(self):
    return Article.objects.all().order_by('-updated_at')

class ArticleDetail(generic.DetailView):
  model = Article

class ArticleCreate(generic.CreateView):
  model = Article
  form_class = ArticleForm
  success_url = reverse_lazy('articles:article-list')

  def form_valid(self, form):
    self.object = form.save(commit=False)
    self.object.user = self.request.user
    self.object.save()
    return super().form_valid(form)

class ArticleUpdate(generic.UpdateView):
  model = Article
  form_class = ArticleForm
  success_url = reverse_lazy('articles:article-list')

class ArticleDelete(generic.DeleteView):
  model = Article
  form_class = ArticleForm
  success_url = reverse_lazy('articles:article-list')
