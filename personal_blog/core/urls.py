from django.urls import path
from .views import Home, About, Terms

app_name = 'core'
urlpatterns = [
  path('', Home.as_view(), name='home'),
  path('about/', About.as_view(), name='about'),
  path('terms/', Terms.as_view(), name='terms'),
]