from django.db import models
from django.contrib.auth.models import User

class Note(models.Model):
  title = models.CharField(max_length=150)
  description = models.TextField()
  user = models.ForeignKey(User, on_delete=models.CASCADE)
  craeted_at = models.DateTimeField(auto_now_add=True)
  updated_at = models.DateTimeField(auto_now=True)
  
  def __str__(self):
    return self.description
