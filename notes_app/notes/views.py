from django.shortcuts import render
from django.contrib.auth.models import User
from django.urls import reverse_lazy
from django.views import generic
from .models import Note
from .forms import NoteForm

class NoteList(generic.ListView):
  model = Note

  def get_queryset(self):
    user = User.objects.get(username=self.request.user.username)
    return Note.objects.filter(user=user).order_by('-updated_at')

class NoteDetail(generic.DetailView):
  model = Note

class NoteCreate(generic.CreateView):
  model = Note
  form_class = NoteForm
  success_url = reverse_lazy('notes:note-list')

  def form_valid(self, form):
    self.object = form.save(commit=False)
    self.object.user = self.request.user
    self.object.save()
    return super().form_valid(form)

class NoteUpdate(generic.UpdateView):
  model = Note
  form_class = NoteForm
  success_url = reverse_lazy('notes:note-list')

class NoteDelete(generic.DeleteView):
  model = Note
  form_class = NoteForm
  success_url = reverse_lazy('notes:note-list')
