from django.urls import path
from . import views

app_name = 'notes'
urlpatterns = [
  path('', views.NoteList.as_view(), name='note-list'),
  path('<int:pk>/', views.NoteDetail.as_view(), name='note-detail'),
  path('create/', views.NoteCreate.as_view(), name='note-create'),
  path('update/<int:pk>/', views.NoteUpdate.as_view(), name='note-update'),
  path('delete/<int:pk>/', views.NoteDelete.as_view(), name='note-delete'),
]
