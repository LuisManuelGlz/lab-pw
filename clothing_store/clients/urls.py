from django.urls import path
from . import views

app_name = 'clients'
urlpatterns = [
  path('', views.ClientList.as_view(), name='client-list'),
  path('<int:pk>/', views.ClientDetail.as_view(), name='client-detail'),
  path('create/', views.ClientCreate.as_view(), name='client-create'),
  path('update/<int:pk>/', views.ClientUpdate.as_view(), name='client-update'),
  path('delete/<int:pk>/', views.ClientDelete.as_view(), name='client-delete'),
]