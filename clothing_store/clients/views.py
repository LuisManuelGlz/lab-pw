from django.shortcuts import render
from django.urls import reverse_lazy
from django.views import generic
from .models import Client
from .forms import ClientForm

class ClientList(generic.ListView):
  model = Client

class ClientDetail(generic.DetailView):
  model = Client

class ClientCreate(generic.CreateView):
  model = Client
  form_class = ClientForm
  success_url = reverse_lazy('clients:client-list')

class ClientUpdate(generic.UpdateView):
  model = Client
  form_class = ClientForm
  success_url = reverse_lazy('clients:client-list')

class ClientDelete(generic.DeleteView):
  model = Client
  form_class = ClientForm
  success_url = reverse_lazy('clients:client-list')
