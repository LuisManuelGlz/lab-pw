from django.db import models
from django.utils import timezone

class Client(models.Model):
  name = models.CharField(max_length=250)
  phone = models.CharField(max_length=10, blank=True, null=True)
  email = models.EmailField(max_length=150, blank=True, null=True)
  created_at = models.DateTimeField(auto_now_add=True)
  updated_at = models.DateTimeField(auto_now=True)
