from django.urls import path
from . import views

app_name = 'products'
urlpatterns = [
  path('', views.ProductList.as_view(), name='product-list'),
  path('<int:pk>/', views.ProductDetail.as_view(), name='product-detail'),
  path('create/', views.ProductCreate.as_view(), name='product-create'),
  path('update/<int:pk>/', views.ProductUpdate.as_view(), name='product-update'),
  path('delete/<int:pk>/', views.ProductDelete.as_view(), name='product-delete'),
]
