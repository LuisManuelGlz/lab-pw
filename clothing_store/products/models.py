from django.db import models

class Product(models.Model):
  name = models.CharField(max_length=250)
  price = models.DecimalField(max_digits=7, decimal_places=2)
  craeted_at = models.DateTimeField(auto_now_add=True, blank=True, null=True)
  updated_at = models.DateTimeField(auto_now=True)
