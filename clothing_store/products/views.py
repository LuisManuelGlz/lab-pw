from django.shortcuts import render
from django.urls import reverse_lazy
from django.views import generic
from .models import Product
from .forms import ProductForm

class ProductList(generic.ListView):
  model = Product

class ProductDetail(generic.DetailView):
  model = Product

class ProductCreate(generic.CreateView):
  model = Product
  form_class = ProductForm
  success_url = reverse_lazy('products:product-list')

class ProductUpdate(generic.UpdateView):
  model = Product
  form_class = ProductForm
  success_url = reverse_lazy('products:product-list')

class ProductDelete(generic.DeleteView):
  model = Product
  form_class = ProductForm
  success_url = reverse_lazy('products:product-list')
