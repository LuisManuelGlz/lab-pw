from django.shortcuts import render
from django.contrib.auth.models import User
from django.urls import reverse_lazy
from django.views import generic
from .models import Post
from .forms import PostForm

class PostList(generic.ListView):
  model = Post

  def get_queryset(self):
    return Post.objects.all().order_by('-updated_at')

class PostDetail(generic.DetailView):
  model = Post

class PostCreate(generic.CreateView):
  model = Post
  form_class = PostForm
  success_url = reverse_lazy('posts:post-list')

  def form_valid(self, form):
    self.object = form.save(commit=False)
    self.object.user = self.request.user
    self.object.save()
    return super().form_valid(form)

class PostUpdate(generic.UpdateView):
  model = Post
  form_class = PostForm
  success_url = reverse_lazy('posts:post-list')

class PostDelete(generic.DeleteView):
  model = Post
  form_class = PostForm
  success_url = reverse_lazy('posts:post-list')
