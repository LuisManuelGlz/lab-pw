from django.urls import path
from . import views

app_name = 'posts'
urlpatterns = [
  path('', views.PostList.as_view(), name='post-list'),
  path('<int:pk>/', views.PostDetail.as_view(), name='post-detail'),
  path('create/', views.PostCreate.as_view(), name='post-create'),
  path('update/<int:pk>/', views.PostUpdate.as_view(), name='post-update'),
  path('delete/<int:pk>/', views.PostDelete.as_view(), name='post-delete'),
]
