from django.shortcuts import render
from django.urls import reverse_lazy
from django.views import generic
from django.contrib.auth.forms import UserCreationForm

class Signup(generic.CreateView):
  template_name = 'registration/signup.html'
  form_class = UserCreationForm
  success_url = reverse_lazy('login')

