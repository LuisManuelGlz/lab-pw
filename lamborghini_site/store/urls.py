from django.urls import path
from . import views

app_name = 'store'
urlpatterns = [
  path('', views.accessories_list, name='store-list'),
  path('<int:id>/', views.accessory_detail, name='store-detail'),
  path('create/', views.accessory_create, name='store-create'),
]