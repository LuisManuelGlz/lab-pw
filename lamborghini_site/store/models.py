from django.db import models

class Accessory(models.Model):
  description = models.CharField(max_length=150)
  price = models.DecimalField(max_digits=7, decimal_places=2)
  created_at = models.DateTimeField(auto_now_add=True)
  updated_at = models.DateTimeField(auto_now=True)
