from django.shortcuts import render, redirect
from .models import Accessory
from .forms import AccessoryForm

def accessories_list(request):
  context = {
    'accessories': Accessory.objects.all()
  }
  return render(request, 'store/store_list.html', context)

def accessory_detail(request, id):
  context = {
    'accessory': Accessory.objects.get(id=id)
  }
  return render(request, 'store/store_detail.html', context)

def accessory_create(request):
  if request.method == 'POST':
    form = AccessoryForm(request.POST)
    if form.is_valid():
      accessory = form.save(commit=False)
      accessory.user = request.user
      accessory.save()
      return redirect('store:store-list')
  else:
      form = AccessoryForm()

  return render(request, 'store/store_form.html', { 'form': form })