from django.urls import path
from . import views

app_name = 'supercars'
urlpatterns = [
  path('', views.SupercarList.as_view(), name='supercar-list'),
  path('<int:pk>/', views.SupercarDetail.as_view(), name='supercar-detail'),
  path('create/', views.SupercarCreate.as_view(), name='supercar-create'),
  path('update/<int:pk>/', views.SupercarUpdate.as_view(), name='supercar-update'),
  path('delete/<int:pk>/', views.SupercarDelete.as_view(), name='supercar-delete'),
]