from django.shortcuts import render
from django.urls import reverse_lazy
from django.views import generic
from .models import Supercar
from .forms import SupercarForm

class SupercarList(generic.ListView):
  model = Supercar

class SupercarDetail(generic.DetailView):
  model = Supercar

class SupercarCreate(generic.CreateView):
  model = Supercar
  form_class = SupercarForm
  success_url = reverse_lazy('supercars:supercar-list')

class SupercarUpdate(generic.UpdateView):
  model = Supercar
  form_class = SupercarForm
  success_url = reverse_lazy('supercars:supercar-list')

class SupercarDelete(generic.DeleteView):
  model = Supercar
  form_class = SupercarForm
  success_url = reverse_lazy('supercars:supercar-list')