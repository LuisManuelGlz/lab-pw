from django import forms
from .models import Supercar

class SupercarForm(forms.ModelForm):
  class Meta:
    model = Supercar
    fields = [
      'model',
      'engine',
      'transmission',
      'speed',
    ]