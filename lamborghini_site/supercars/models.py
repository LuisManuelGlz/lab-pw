from django.db import models

class Supercar(models.Model):
  model = models.CharField(max_length=100)
  engine = models.CharField(max_length=250)
  transmission = models.CharField(max_length=250)
  speed = models.DecimalField(max_digits=5, decimal_places=2)
  created_at = models.DateTimeField(auto_now_add=True)
  updated_at = models.DateTimeField(auto_now=True)