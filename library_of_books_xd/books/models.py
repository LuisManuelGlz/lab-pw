from django.db import models
from authors.models import Author

class Book(models.Model):
  title = models.CharField(max_length=150)
  description = models.TextField()
  price = models.DecimalField(max_digits=6, decimal_places=2)
  author = models.ManyToManyField(Author, related_name='books')

  def __str__(self):
    return self.title