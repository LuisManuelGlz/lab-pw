from django.urls import path
from . import views

app_name = 'books'
urlpatterns = [
  path('', views.BookList.as_view(), name='book-list'),
  path('<int:pk>/', views.BookDetail.as_view(), name='book-detail'),
  path('create/', views.BookCreate.as_view(), name='book-create'),
  path('update/<int:pk>/', views.BookUpdate.as_view(), name='book-update'),
  path('delete/<int:pk>/', views.BookDelete.as_view(), name='book-delete'),
]