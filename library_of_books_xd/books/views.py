from django.shortcuts import render
from django.urls import reverse_lazy
from .models import Book
from .forms import BookForm
from django.views import generic

class BookList(generic.ListView):
  model = Book

class BookDetail(generic.DetailView):
  model = Book

class BookCreate(generic.CreateView):
  model = Book
  form_class = BookForm
  success_url = reverse_lazy('books:book-list')

class BookUpdate(generic.UpdateView):
  model = Book
  form_class = BookForm
  success_url = reverse_lazy('books:book-list')

class BookDelete(generic.DeleteView):
  model = Book
  form_class = BookForm
  success_url = reverse_lazy('books:book-list')