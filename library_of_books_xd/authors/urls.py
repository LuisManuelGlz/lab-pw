from django.urls import path
from . import views

app_name = 'authors'
urlpatterns = [
  path('', views.AuthorList.as_view(), name='author-list'),
  path('<int:pk>/', views.AuthorDetail.as_view(), name='author-detail'),
  path('create/', views.AuthorCreate.as_view(), name='author-create'),
  path('update/<int:pk>/', views.AuthorUpdate.as_view(), name='author-update'),
  path('delete/<int:pk>/', views.AuthorDelete.as_view(), name='author-delete'),
]