from django.shortcuts import render
from django.urls import reverse_lazy
from .models import Author
from .forms import AuthorForm
from django.views import generic

class AuthorList(generic.ListView):
  model = Author

class AuthorDetail(generic.DetailView):
  model = Author

class AuthorCreate(generic.CreateView):
  model = Author
  form_class = AuthorForm
  success_url = reverse_lazy('authors:author-list')

class AuthorUpdate(generic.UpdateView):
  model = Author
  form_class = AuthorForm
  success_url = reverse_lazy('authors:author-list')

class AuthorDelete(generic.DeleteView):
  model = Author
  form_class = AuthorForm
  success_url = reverse_lazy('authors:author-list')